import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormService } from 'src/app/services/form';
import swal from 'sweetalert2';
import { FormControl, FormGroupDirective, NgForm, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserMS } from '../model/login';
import { ErrorStateMatcher } from '@angular/material/core';
import { LoginService } from '../services/login.service';
import { ToastrService } from 'ngx-toastr';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}
@Component({
  selector: 'app-cambiar-contrasena',
  templateUrl: './cambiar-contrasena.component.html',
  providers: [LoginService]
})
export class CambiarContrasenaComponent {
  cambiarForm: FormGroup;
  matcher = new MyErrorStateMatcher();
  public type = "password";
  public type2 = "password";
  public type3 = "password";
  public user: UserMS;
  constructor(public fb: FormBuilder,
    public FormService: FormService,
    public toastr: ToastrService,
    private usuarioService: LoginService,
    private router: Router, private activatedRoute: ActivatedRoute,
  ) {
    this.user = JSON.parse(window.localStorage.getItem("user"));
    this.cambiarForm = this.fb.group({
      old_password: ['', [Validators.required]],
      new_password: ['', [Validators.required,  Validators.pattern("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[A-Za-z\\d]{6,15}$")]],
      conf_password: ['', [Validators.required,  Validators.pattern("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[A-Za-z\\d]{6,15}$")]]
    });
  }


  enter(event) {
    if (event == 'type') {
      this.type = "text";
    }
    if (event == 'type2') {
      this.type2 = 'text';
    }
    if (event == "type3") {
      this.type3 = 'text';
    }
  }
  leave(event) {
    if (event == "type") {
      this.type = 'password';
    }
    if (event == "type2") {
      this.type2 = 'password';
    }
    if (event == "type3") {
      this.type3 = 'password';
    }
  }
  public navigate(nav) {
    this.router.navigate(nav, { relativeTo: this.activatedRoute });
  }
  public disable;
  change() {
    this.FormService.markFormGroupTouched(this.cambiarForm);
    let params = {
      CodUsuario: this.user.user_id,
      ContraseñaAnterior: this.cambiarForm.value["old_password"],
      NuevaContraseña: this.cambiarForm.value["new_password"]
    }
    console.log(params);
    if (this.cambiarForm.valid) {
      if (this.cambiarForm.value["new_password"] != this.cambiarForm.value["conf_password"]) {
        this.toastr.info("CONFIRMAR CORRECTAMENTE LA CONTRASEÑA");
        return;
      }
      else {
        this.disable = true;
        this.usuarioService.CambiarContraseña(params).subscribe((res) => {
          this.disable = false;
          if(res.status==0){
            this.toastr.success((res.message_error).toUpperCase());
          }
          else{
            this.toastr.info((res.message_error).toUpperCase());
          }
        });
      }
    }

  }


}

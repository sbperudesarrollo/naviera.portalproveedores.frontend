import { Routes } from '@angular/router';
import { CambiarContrasenaComponent } from './cambiar-contrasena.component';


export const MenuRoutes: Routes = [
  {
    path: '',
    children: [
    {
      path: 'cambiarcontrasena',
      component: CambiarContrasenaComponent
    }
  ]
  }
];

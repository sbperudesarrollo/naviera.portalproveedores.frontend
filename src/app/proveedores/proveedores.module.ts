import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
//import {UtilsModule} from '../utils/utils.module';
import { MaterialModule } from '../app.module';



import { FormService } from '../services/form';

import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { ProveedoresRoutes } from './proveedores.routing';
import { ProveedoresBuscarComponent } from './buscar/proveedoresbuscar.component';

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(ProveedoresRoutes),
        FormsModule,
        ReactiveFormsModule,
        MaterialModule,
        MatCardModule,
        MatInputModule,
        MatButtonModule
    ],
    providers: [FormService],
    declarations: [
        ProveedoresBuscarComponent],
})

export class ProveedoresModule { }

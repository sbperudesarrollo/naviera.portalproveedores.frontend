import { Routes } from '@angular/router';
import { ProveedoresBuscarComponent } from './buscar/proveedoresbuscar.component';
export const ProveedoresRoutes: Routes = [
  {

    path: '',
    children: [
     
     {
        path: 'buscar',
        component: ProveedoresBuscarComponent
      }  
    ]
  }
];

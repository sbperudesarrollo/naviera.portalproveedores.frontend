import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit, AfterViewInit } from '@angular/core';
import { AuthGuardService } from 'src/app/services/auth-guard.service';
import { UserMS } from 'src/app/model/login';
import { URL_RUC_BUSCAR } from 'src/app/utils/app.constants';
import * as moment from 'moment';

declare var $: any;
var oComponent: ProveedoresBuscarComponent;
var dtResultadoProveedor;

@Component({
  moduleId: module.id,
  selector: 'proveedoresbuscar-cmp',
  templateUrl: 'proveedoresbuscar.component.html',
})
export class ProveedoresBuscarComponent implements OnInit, AfterViewInit {
  public user: UserMS;

  constructor(private router: Router, private activatedRoute: ActivatedRoute, public auth: AuthGuardService) {
    this.user = JSON.parse(window.localStorage.getItem("user")) as UserMS;

  }
  private ConfigurarBuscar() {
    dtResultadoProveedor = $('#dtResultadoProveedor').on('init.dt', function (e, settings, json) {
    }).DataTable({
      processing: true,
      serverSide: true,
      ajax: {
        type: "POST",
        beforeSend: function (request) {
          if (oComponent.auth.isTokenExpired(oComponent.user.token)) { window.location.href = './pages/login'; };
          request.setRequestHeader("Authorization", 'Bearer ' + oComponent.user.token);
        },
        url: URL_RUC_BUSCAR,

        data: function (d) {

        }
      },
      columns: [

        { data: 'RUC' },
        { data: 'RazonSocial' },
        { data: 'Apellido' },
        { data: 'Correo1' },
        { data: 'FechaCreacion' },
        { data: 'Estado' }
      ],
      columnDefs: [
        { 'className': 'text-center', 'targets': [0, 1, 2, 3, 4, 5] },
        {
          render: function (data, type, row) {
            return '<span style="text-transform: uppercase;">' + row.RazonSocial + '</span>';
          },
          targets: 1
        },
        {
          render: function (data, type, row) {
            return '<span style="text-transform: uppercase;">' + row.Apellido + '</span>';
          },
          targets: 2
        },
        {
          render: function (data, type, row) {
            return '<span style="text-transform: uppercase;">' + row.Correo1 + '</span>';
          },
          targets: 3
        },
        {
          render: function (data, type, row) {
              if (row.FechaCreacion == '' || row.FechaCreacion == null) { return "" };
              var actionsHtml = moment(row.FechaCreacion).format("DD/MM/YYYY HH:mm");
              return actionsHtml;
          },
          targets: 4
      },

      ]

    });


  }
  ngAfterViewInit() {

    this.ConfigurarBuscar();

  }


  public navigate(nav) {
    this.router.navigate(nav, { relativeTo: this.activatedRoute });
  }

  ngOnInit() {
    oComponent = this;
  }

}


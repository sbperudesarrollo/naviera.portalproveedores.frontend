import { Injectable } from '@angular/core';
import { ServicesUtils } from '../utils/services.utils';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { URL_ORDEN_PAGO, URL_FACTURA, URL_ORDEN_PDF, URL_ORDEN_PDF2 } from '../utils/app.constants';

@Injectable({
  providedIn: 'root'
})
export class OrdenPagoService {

  constructor(private http: HttpClient, private settingsU: ServicesUtils) { }

  GetOrdenPago(item: any): Observable<any> {
    let items$ = this.http
      .post(URL_ORDEN_PAGO, item, { headers: this.settingsU.getHeader() })
      .pipe(map(this.settingsU.extractData))
      ;
    return items$;
  }
  GetFacturas(item: any): Observable<any> {
    let items$ = this.http
      .post(URL_FACTURA, item, { headers: this.settingsU.getHeader() })
      .pipe(map(this.settingsU.extractData))
      ;
    return items$;
  }
  GetPdf(item: any): Observable<any> {
    let items$ = this.http
      .post(URL_ORDEN_PDF, item, { headers: this.settingsU.getHeader() })
      .pipe(map(this.settingsU.extractData))
      ;
    return items$;
  }
  GetPdf2(item: any): Observable<any> {
    let items$ = this.http
      .post(URL_ORDEN_PDF2, item, { headers: this.settingsU.getHeader() })
      .pipe(map(this.settingsU.extractData))
      ;
    return items$;
  }
}

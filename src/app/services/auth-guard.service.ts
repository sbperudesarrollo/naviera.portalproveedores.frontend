import { Injectable, NgZone } from '@angular/core';
import { Router, CanActivate, CanActivateChild, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
//import { LoginService } from './login.service';
import { Observable, of, throwError } from 'rxjs';
import { UserMS } from 'src/app/model/login';
import * as jwt_decode from 'jwt-decode';

declare var DatatableFunctions: any, GlobalFunctions: any;

@Injectable()
export class AuthGuardService implements CanActivate, CanActivateChild {

    constructor(/*public auth: LoginService, */public router: Router,private ngZone:NgZone) {
    }

    canActivate(): boolean {
        // GlobalFunctions.setup(true, TIME_INACTIVE);
        // DatatableFunctions.ConnectWebsockets(URL_CONSUMER);

        return this.verificarLogeo();
    }

    canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        return this.verificarLogeo();
    }

    verificarLogeo(): boolean {
        if (window.localStorage.getItem("user") != null) {

            let user = JSON.parse(window.localStorage.getItem("user")) as UserMS;

            let token = user.token;

            if (this.isTokenExpired(token)) {

                setTimeout(
                    () => {
                        window.location.href = '/pages/login';
                        return false;
                    }, 500);
            }
            else return true;

        }


        return false;
    }

    getTokenExpirationDate(token: string): Date {
        const decoded = jwt_decode(token);

        if (decoded.exp === undefined) return null;

        const date = new Date(0);
        date.setUTCSeconds(decoded.exp);
        return date;
    }

    isTokenExpired(token?: string): boolean {

        if (!token) return true;

        const date = this.getTokenExpirationDate(token);
        if (date === undefined) return false;
        return !(date.valueOf() > new Date().valueOf());
    }
}
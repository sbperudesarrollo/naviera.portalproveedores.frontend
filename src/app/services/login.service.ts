import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ServicesUtils } from '../utils/services.utils'

import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import { LoginMS } from 'src/app/model/login';
import { URL_LOGIN, URL_RUC, URL_RUC_PROVEEDOR, URL_REGISTRO, URL_RECUPERAR, URL_CAMBIAR_PASS } from '../utils/app.constants';


@Injectable()
export class LoginService {

    constructor(private http: HttpClient, private settingsU: ServicesUtils) { }

    LogIn(user: LoginMS): Observable<any> {
        var oJSON = JSON.stringify(user);
        let items$ = this.http
            .post(URL_LOGIN, user, { headers: this.settingsU.getHeaderLogin() })
            .pipe(map(this.settingsU.extractData));
        return items$;
    }
    RUC(body: String): Observable<any> {
        let items$ = this.http
            .get(URL_RUC + body, { headers: this.settingsU.getHeaderLogin() })
            .pipe(map(this.settingsU.extractData));
        return items$;
    }
    SolicitarClave(params: any): Observable<any> {
        let items$ = this.http
            .post(URL_RUC, params, { headers: this.settingsU.getHeaderLogin() })
            .pipe(map(this.settingsU.extractData));
        return items$;
    }
    GetProveedor(params: any): Observable<any> {
        let items$ = this.http
            .post(URL_RUC_PROVEEDOR, params, { headers: this.settingsU.getHeaderLogin() })
            .pipe(map(this.settingsU.extractData));
        return items$;
    }
    Update(params: any): Observable<any> {
        let items$ = this.http
            .put(URL_RUC, params, { headers: this.settingsU.getHeaderLogin() })
            .pipe(map(this.settingsU.extractData));

        return items$;
    }

    RegistroProveedor(params: any): Observable<any> {
        let items$ = this.http
            .post(URL_REGISTRO, params, { headers: this.settingsU.getHeaderLogin() })
            .pipe(map(this.settingsU.extractData));
        return items$;
    }
    RecuperarContraseña(params: any): Observable<any> {
        let items$ = this.http
            .post(URL_RECUPERAR, params, { headers: this.settingsU.getHeaderLogin() })
            .pipe(map(this.settingsU.extractData));
        return items$;
    }
    CambiarContraseña(params: any): Observable<any> {
        let items$ = this.http
            .post(URL_CAMBIAR_PASS, params, { headers: this.settingsU.getHeaderLogin() })
            .pipe(map(this.settingsU.extractData));
        return items$;
    }
}
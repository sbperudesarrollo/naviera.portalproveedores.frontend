import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroupDirective, NgForm, FormGroup, Validators } from '@angular/forms';
import { ErrorStateMatcher, DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { FormBuilder } from '@angular/forms';
import { FormService } from 'src/app/services/form';
import { UserMS } from 'src/app/model/login';

import { AuthGuardService } from 'src/app/services/auth-guard.service';
import { OrdenPagoService } from 'src/app/services/ordenpago.service';
import { ToastrService } from 'ngx-toastr';
import { AppDateAdapter, APP_DATE_FORMATS } from 'src/app/utils/format-datepicker';

export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

var oComponent: EstadoFacturaBuscarComponent;
declare var $: any;
declare var moment: any;

@Component({
  moduleId: module.id,
  selector: 'estadofacturabuscar-cmp',
  templateUrl: 'estadofacturabuscar.component.html',
  providers: [OrdenPagoService]

})
export class EstadoFacturaBuscarComponent implements OnInit {

  filtrosForm: FormGroup;
  matcher = new MyErrorStateMatcher();
  public IDPerfil;
  public RUC = null;
  public user: UserMS;
  public Today = new Date();
  public FeIni = new Date();
  public List = [];
  public ListAux = [];
  public disable = false;
  public clase4;
  public porcentaje;
  constructor(private router: Router, private activatedRoute: ActivatedRoute, public orden: OrdenPagoService,
    public toastr: ToastrService,
    public fb: FormBuilder, public FormService: FormService, public auth: AuthGuardService) {
    if (window.screen.width > 1000) {
      this.clase4 = "table table-no-bordered table-striped"

    }
    else {
      this.clase4 = "table table-no-bordered table-striped table-responsive-stack"
    }
    this.user = JSON.parse(window.localStorage.getItem("user")) as UserMS;
    this.IDPerfil = this.user.user.CodRol;
  }

  CrearFormulario() {
    this.filtrosForm = this.fb.group({
      RUC: [{ value: oComponent.RUC ? oComponent.RUC : '', disabled: false }, [Validators.required, Validators.pattern("^[0-9]{11}$")]],
      Factura: ['', []],
      FechaInicio: [{ value: oComponent.FeIni ? moment(oComponent.FeIni).format('YYYY-MM-DD') : '', disabled: false }, []],
      FechaFin: [{ value: oComponent.Today ? moment(oComponent.Today).format('YYYY-MM-DD') : '', disabled: false }, []],

    });
  }
  public navigate(nav) {
    this.router.navigate(nav, { relativeTo: this.activatedRoute });
  }

  ngOnInit() {
    oComponent = this;
    this.FeIni.setDate(this.Today.getDate() - 30);
    if (this.IDPerfil != '0022') {
      this.RUC = this.user.user_id;
      this.CrearFormulario();
      this.Buscar();
    }
    else {
      this.CrearFormulario();
    }
  }

  Buscar() {
    let diferencia;
    var fecha1 = moment(oComponent.filtrosForm.value["FechaInicio"]);
    var fecha2 = moment(oComponent.filtrosForm.value["FechaFin"]);
    diferencia = fecha2.diff(fecha1, 'days');
    this.List = [];
    let item = {
      Ruc: oComponent.filtrosForm.value["RUC"] ? oComponent.filtrosForm.value["RUC"] : this.RUC,
      Factura: oComponent.filtrosForm.value["Factura"] ? '01-' + oComponent.filtrosForm.value["Factura"] : '',
      FechaInicio: oComponent.filtrosForm.value["FechaInicio"] ? moment(oComponent.filtrosForm.value["FechaInicio"]).format('DD/MM/YYYY') : moment(oComponent.FeIni).format('DD/MM/YYYY'),
      FechaFin: oComponent.filtrosForm.value["FechaFin"] ? moment(oComponent.filtrosForm.value["FechaFin"]).format('DD/MM/YYYY') : moment(oComponent.Today).format('DD/MM/YYYY'),
    }
    this.FormService.markFormGroupTouched(this.filtrosForm);
    if (this.filtrosForm.valid) {
      if (diferencia > 31) {
        this.toastr.info(("El rango de fechas no debe ser mayor a 31 días calendarios").toUpperCase());
        return;
      }
      else {
        this.disable = true;
        this.orden.GetFacturas(item).subscribe((res) => {
          if (window.screen.width < 1000) {
            this.inicio();
          }
          this.ListAux = res;
          if (this.ListAux.length == undefined) {
            this.toastr.info("NO EXISTEN REGISTROS QUE MOSTRAR");
          }
          else if (this.ListAux[0].stcd1 == "99999999991") {
            this.toastr.info("EL RUC NO EXISTE");

          }
          else if (this.ListAux[0].stcd1 == "99999999992") {
            this.toastr.info("NO EXISTEN REGISTROS QUE MOSTRAR")
          }
          else {
            this.ListAux.forEach(element => {
              element["fechaEmision"] = moment(element.bldat).format("DD/MM/YYYY");
              this.List.push(element);
            });
          }
          this.disable = false;
        });
      }
    }
  }
  limpiar() {
    if (this.IDPerfil != '0022') {
      this.RUC = this.user.user_id;
      this.CrearFormulario();
    }
    else {
      this.filtrosForm = this.fb.group({
        RUC: [{ value: '', disabled: false }, [Validators.required, Validators.pattern("^[0-9]{11}$")]],
        Factura: ['', []],
        FechaInicio: [{ value: oComponent.FeIni ? moment(oComponent.FeIni).format('YYYY-MM-DD') : '', disabled: false }, []],
        FechaFin: [{ value: oComponent.Today ? moment(oComponent.Today).format('YYYY-MM-DD') : '', disabled: false }, []],
      });
    }
  }
  public inicio() {
    $(document).ready(function () {
      $('.table-responsive-stack').find("th").each(function (i) {
        $('.table-responsive-stack td:nth-child(' + (i + 1) + ')').prepend('<span class="table-responsive-stack-thead">' + $(this).text() + ':</span> ');
        $('.table-responsive-stack-thead').hide();
      });
      $('.table-responsive-stack').each(function () {
        var thCount = $(this).find("th").length;
        var rowGrow = 100 / thCount + '%';
        $(this).find("th, td").css('flex-basis', rowGrow);
      });
      function flexTable() {
        if ($(window).width() < 768) {
          $(".table-responsive-stack").each(function (i) {
            $(this).find(".table-responsive-stack-thead").show();
            $(this).find('thead').hide();
          });
        }
        else {
          $(".table-responsive-stack").each(function (i) {
            $(this).find(".table-responsive-stack-thead").hide();
            $(this).find('thead').show();
          });
        }
        // flextable   
      }
      flexTable();
      window.onresize = function (event) {
        flexTable();
      };
      // document ready  
    });
  }
}


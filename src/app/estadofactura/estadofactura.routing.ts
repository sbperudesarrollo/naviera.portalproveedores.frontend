import { Routes } from '@angular/router';
import { EstadoFacturaBuscarComponent } from './buscar/estadofacturabuscar.component';
export const EstadoFacturaRoutes: Routes = [
  {

    path: '',
    children: [
     
     {
        path: 'buscar',
        component: EstadoFacturaBuscarComponent
      }  
    ]
  }
];

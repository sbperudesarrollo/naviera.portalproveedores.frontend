import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
//import {UtilsModule} from '../utils/utils.module';
import { MaterialModule } from '../app.module';



import { FormService } from '../services/form';

import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { EstadoFacturaRoutes } from './estadofactura.routing';
import { EstadoFacturaBuscarComponent } from './buscar/estadofacturabuscar.component';
import { DateAdapter, MAT_DATE_LOCALE, MAT_DATE_FORMATS } from '@angular/material/core';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
export const MY_FORMATS = {
    parse: {
        dateInput: 'DD/MM/YYYY',
    },
    display: {
        dateInput: 'DD/MM/YYYY',
        monthYearLabel: 'MMM YYYY',
        dateA11yLabel: 'DD/MM/YYYY',
        monthYearA11yLabel: 'MMMM YYYY',
    },
};
@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(EstadoFacturaRoutes),
        FormsModule,
        ReactiveFormsModule,
        MaterialModule,
        MatCardModule,
        MatInputModule,
        MatButtonModule
    ],
    providers: [FormService,
        { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
        { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS }],    declarations: [
        EstadoFacturaBuscarComponent],
})

export class EstadoFacturaModule { }

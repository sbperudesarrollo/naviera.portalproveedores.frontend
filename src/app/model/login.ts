export class LoginMS {

    public username: string;
    public password: string;
    public confirmar:string;
    constructor() {
        this.username='';
        this.password='';
        this.confirmar='';
    }
}


export class UserMS {
    public user_id: string;
    public user: User;
    public token: string;
    constructor() {
        this.user = new User();

    }

}

export class User{
    public CodRol;
    public CodUsuario;
    public NroDocumento;
    public CodRolNombre;
    public Correo;
    public Nombres;
    public Apellidos;
}


export class PermisoMS {

    public modulo: string;
    public enviarmail: number;
    public enviarsms: number;

    public acciones: AccionMS[];
    constructor() {
       
        this.acciones = [];
    }
}

export class AccionMS {

    public accion: string;
    public disponible: number;
}
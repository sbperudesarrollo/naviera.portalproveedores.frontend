import { Component, OnInit } from '@angular/core';
import PerfectScrollbar from 'perfect-scrollbar';
import { UserMS } from '../model/login';
import { Router, ActivatedRoute } from '@angular/router';
declare const $: any;

//Metadata
export interface RouteInfo {
    path: string;
    title: string;
    type: string;
    icontype: string;
    collapse?: string;
    children?: ChildrenItems[];
}

export interface ChildrenItems {
    path: string;
    title: string;
    ab: string;
    type?: string;
}

//Menu Items
export const ROUTES: RouteInfo[] = [];
@Component({
    selector: 'app-sidebar-cmp',
    templateUrl: 'sidebar.component.html',
})

export class SidebarComponent implements OnInit {
    public menuItems: any[];
    public razonSocial;
    public tipo;
    public perfil;
    ps: any;
    constructor(private router: Router, private activatedRoute: ActivatedRoute) {

    }
    isMobileMenu() {
        if ($(window).width() > 991) {
            return false;
        }
        return true;
    };

    ngOnInit() {
        if (window.matchMedia(`(min-width: 960px)`).matches && !this.isMac()) {
            const elemSidebar = <HTMLElement>document.querySelector('.sidebar .sidebar-wrapper');
            this.ps = new PerfectScrollbar(elemSidebar);
        }
        let user = JSON.parse(window.localStorage.getItem("user")) as UserMS;
        if(user.user.Nombres.length>21){
            this.razonSocial = (user.user.Nombres).substr(0,20) + '…';
        }
        else{
            this.razonSocial = user.user.Nombres;
        }
        this.perfil = user.user.CodRol;
        this.menuItems = ROUTES.filter(menuItem => menuItem);
        this.menuItems = [];

        this.menuItems.push({
            path: '/ordenpago/buscar/1',
            title: 'Órdenes de pago',
            type: 'link',
            clase : 'nav-link order ng-star-inserted'
        });
        this.menuItems.push({
            path: '/estadofactura/buscar',
            title: 'Estado de facturas',
            type: 'link',
            icontype: 'folder',
            clase : 'nav-link invoice ng-star-inserted'
        });

        if (this.perfil == '0022') {
            this.menuItems.push({
                path: '/proveedores/buscar',
                title: 'Proveedores registrados',
                type: 'link',
                clase : 'nav-link provider ng-star-inserted'
            });
        }
        else {
            this.tipo = user.user_id;
            this.menuItems.push({
                path: '/infousuario/formulario',
                title: 'Información de usuario',
                type: 'link',
                clase : 'nav-link provider ng-star-inserted'
            });
        }
        this.menuItems.push({
            path: '/ayuda/formulario',
            title: 'Ayuda',
            type: 'link',
            clase : 'nav-link help ng-star-inserted'
        });
    }
    updatePS(): void {
        if (window.matchMedia(`(min-width: 960px)`).matches && !this.isMac()) {
            this.ps.update();
        }
    }
    isMac(): boolean {
        let bool = false;
        if (navigator.platform.toUpperCase().indexOf('MAC') >= 0 || navigator.platform.toUpperCase().indexOf('IPAD') >= 0) {
            bool = true;
        }
        return bool;
    }
    public navigate(nav) {
        this.router.navigate(nav, { relativeTo: this.activatedRoute });
    }
    cerrarSesion() {
        localStorage.removeItem('user');
        let nav = ["/pages/login/"];
        this.navigate(nav);
    }
    ChangePass() {
        this.router.navigate(['/menu/cambiarcontrasena']);
    }
}

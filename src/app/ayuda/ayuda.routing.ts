import { Routes } from '@angular/router';
import { AyudaComponent } from './formulario/ayuda.component';
export const AyudaRoutes: Routes = [
  {

    path: '',
    children: [
     
     {
        path: 'formulario',
        component: AyudaComponent
      }  
    ]
  }
];

import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { UserMS } from 'src/app/model/login';
import { AuthGuardService } from 'src/app/services/auth-guard.service';


var oComponent: AyudaComponent;

@Component({
  moduleId: module.id,
  selector: 'ayuda-cmp',
  templateUrl: 'ayuda.component.html',
})
export class AyudaComponent implements OnInit {

  public user: UserMS;

  constructor(private router: Router, private activatedRoute: ActivatedRoute,
    public auth: AuthGuardService) {
    this.user = JSON.parse(window.localStorage.getItem("user")) as UserMS;
  }

  public navigate(nav) {
    this.router.navigate(nav, { relativeTo: this.activatedRoute });
  }
  ngOnInit() {
    oComponent = this;
  }


}


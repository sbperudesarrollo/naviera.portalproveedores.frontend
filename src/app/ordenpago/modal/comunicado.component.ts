import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
declare var $: any;

@Component({
  moduleId: module.id,
  selector: 'comunicado-cmp',
  templateUrl: 'comunicado.component.html',
})
export class ComunicadoComponent implements OnInit {
  public Empresas = [
    { nombre: "Naviera Transoceánica S.A", correo: "facturacionreceptor1@navitranso.com", ruc: "20522163890" },
    { nombre: "Petrolera Transoceánica S.A.", correo: "facturacionreceptor@petranso.com", ruc: "20100126606" },
    { nombre: "Naviera Petral S.A", correo: "facturacionreceptor@petral.com.pe", ruc: "20511922578" }
  ]

  constructor(public _modalDialog: MatDialog,
    public dialogRef: MatDialogRef<ComunicadoComponent>) {

  }

  ngOnInit() {

  }
  close(ad?) {
    this.dialogRef.close(ad);
  }
}


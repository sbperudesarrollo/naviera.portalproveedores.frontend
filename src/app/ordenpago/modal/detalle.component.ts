import { Component, OnInit, Input } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { OrdenPagoService } from 'src/app/services/ordenpago.service';
import { ToastrService } from 'ngx-toastr';
import { NgBlockUI, BlockUI } from 'ng-block-ui';
import { ModalPdfComponent } from './modal-pdf.component';

declare var $: any;
declare var moment: any;

@Component({
  moduleId: module.id,
  selector: 'detalle-cmp',
  templateUrl: 'detalle.component.html',
  providers: [OrdenPagoService]

})
export class DetalleComponent implements OnInit {
  @Input() op;
  @Input() aux;
  public imprimir = {
    bldat: null,
    nom_adqui: null,
    ruc_adqui: null,
    doc_det: {
      const_detrac: null,
      cta_detrac: null,
      ruc_provee: null,
      nom_provee: null,
      tipo_doc: null,
      num_doc: null,
      tq: null,
      tbs: null,
      porc_det: null,
      mont_det_sol: null,
      mont_det_dol: null
    }
  }
  public show;
  public ListDetalle = [];
  public fechaEmision;
  @BlockUI() blockUI: NgBlockUI;

  constructor(public _modalDialog: MatDialog,
    public dialogRef: MatDialogRef<DetalleComponent>,
    public orden: OrdenPagoService,
    public toastr: ToastrService,) {
    if (window.screen.width > 1000) {
      this.show = 0;
    }
    else {
      this.inicio();
      this.show = 1;
    }
  }

  public inicio() {
    $(document).ready(function () {
      $('.table-responsive-stack2').find("th").each(function (i) {
        $('.table-responsive-stack2 td:nth-child(' + (i + 1) + ')').prepend('<span class="table-responsive-stack2-thead">' + $(this).text() + ':</span> ');
        $('.table-responsive-stack2-thead').hide();
      });
      $('.table-responsive-stack2').each(function () {
        var thCount = $(this).find("th").length;
        var rowGrow = 100 / thCount + '%';
        $(this).find("th, td").css('flex-basis', rowGrow);
      });
      function flexTable() {
        if ($(window).width() < 768) {
          $(".table-responsive-stack2").each(function (i) {
            $(this).find(".table-responsive-stack2-thead").show();
            $(this).find('thead').hide();
          });
        }
        else {
          $(".table-responsive-stack2").each(function (i) {
            $(this).find(".table-responsive-stack2-thead").hide();
            $(this).find('thead').show();
          });
        }
        // flextable   
      }
      flexTable();
      window.onresize = function (event) {
        flexTable();
      };
      // document ready  
    });
  };
  ngOnInit() {
    this.Detalle();
  }
  public Detalle() {
    this.ListDetalle = [];
    this.fechaEmision = moment(this.op.zaldt).format("DD/MM/YYYY");
    var o = JSON.parse(this.op.detalle);
    this.ListDetalle = o;
    this.ListDetalle.forEach(function (element) {
      element.bldat = moment(element.bldat).format("DD/MM/YYYY");
    });
  }
  public imprimirDetraccion(imprimir) {
    this.imprimir.bldat = this.op.fechaEmision,
      this.imprimir.nom_adqui = this.op.nom_adqui,
      this.imprimir.ruc_adqui = this.op.ruc_adqui,
      this.imprimir.doc_det = imprimir.doc_det;
    this.toastr.info('EL PRESENTE DOCUMENTO ES SÓLO PARA FINES INFORMATIVOS, SI UD. DESEA OBTENER SU CONSTANCIA DE DETRACCIÓN PUEDE INGRESAR A SUNAT CON SU CLAVE SOL.', '', {
      positionClass: 'toast-bottom-right'
    });
    $('#print-info').printThis({});
  };
  close(ad?) {
    this.dialogRef.close(ad);
  }
  public Nombrepdf;
  public pdf(e) {
    this.blockUI.start('Cargando...'); // Start blocking
    var contador = e.xblnr.length;
    this.Nombrepdf = this.op.ruc_adqui + '-20-' + e.comp_reten + '.pdf';
    var item = {
      NombrePdf: this.Nombrepdf,
      Sociedad: this.op.zbukr,
      RUC: this.aux.RUC,
      FechaInicio: this.aux.FechaInicio,
      FechaFin: this.aux.FechaFin,
      NumeroDocumento: (e.xblnr).substr(0, 3) + (e.xblnr).substr(8, contador),
      ComprobanteDeRetencion: e.comp_reten
    };
    this.orden.GetPdf(item).subscribe((res) => {
      if (res.status == 0) {
        var archivo = "data:application/pdf;base64," + res.message_error;
        this.blockUI.stop(); // Stop blocking
        if (window.screen.width < 1000) {
          var linkSource = archivo;
          var downloadLink = document.createElement("a");
          var fileName = this.Nombrepdf;
          downloadLink.href = linkSource;
          downloadLink.download = fileName;
          downloadLink.click();
        }
        else {
          this.openModal(archivo);
        }
      }
      else {
        this.pdf2(res.retencion_pdf);
        this.blockUI.stop(); // Stop blocking
      }
    });
  }
  public pdf2(e) {
    var _this = this;
    var item = {
      pdf_key: e,
      tipo: "DR",
      FechaInicio: this.aux.FechaInicio
    };
    this.orden.GetPdf2(item).subscribe((res) => {
      if (res.file != null) {
        var archivo = "data:application/pdf;base64," + res.file;
        if (window.screen.width < 1000) {
          var linkSource = archivo;
          var downloadLink = document.createElement("a");
          var fileName = _this.Nombrepdf;
          downloadLink.href = linkSource;
          downloadLink.download = fileName;
          downloadLink.click();
        }
        else {
          this.openModal(archivo);
        }
      }
      else {
        this.toastr.info(("EL ARCHIVO DE COMPROBANTE DE RETENCIÓN NO FUE ENCONTRADO").toUpperCase());
      }
    });
  }
  public openModal(mystring) {
    var dialogRef = this._modalDialog.open(ModalPdfComponent, {
      autoFocus: false,
      width: '48%',
      height: '75%',
      panelClass: 'pdfs'
    });
    dialogRef.componentInstance.mystring = mystring;
    dialogRef.afterClosed().subscribe((result) => {
    });
  };
}


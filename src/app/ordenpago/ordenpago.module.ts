import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
//import {UtilsModule} from '../utils/utils.module';
import { MaterialModule } from '../app.module';



import { FormService } from '../services/form';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { OrdenPagoRoutes } from './ordenpago.routing';
import { OrdenPagoBuscarComponent } from './buscar/ordenpagobuscar.component';
import { MAT_DATE_FORMATS, DateAdapter, MAT_DATE_LOCALE } from '@angular/material/core';
import { BlockUIModule } from 'ng-block-ui';
import { BlockUIHttpModule } from 'ng-block-ui/http';
import { ComunicadoComponent } from './modal/comunicado.component';
import { DetalleComponent } from './modal/detalle.component';
import { ModalPdfComponent } from './modal/modal-pdf.component';
export const MY_FORMATS = {
    parse: {
        dateInput: 'DD/MM/YYYY',
    },
    display: {
        dateInput: 'DD/MM/YYYY',
        monthYearLabel: 'MMM YYYY',
        dateA11yLabel: 'DD/MM/YYYY',
        monthYearA11yLabel: 'MMMM YYYY',
    },
};
@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(OrdenPagoRoutes),
        FormsModule,
        ReactiveFormsModule,
        MaterialModule,
        MatCardModule,
        MatInputModule,
        MatButtonModule,
        BlockUIModule.forRoot({
            message: "Cargando..."
          }),
          BlockUIHttpModule.forRoot(),
    ],
    providers: [FormService,
        { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
        { provide: MAT_DATE_FORMATS, useValue: MY_FORMATS }],
    declarations: [
        OrdenPagoBuscarComponent,ComunicadoComponent,DetalleComponent,ModalPdfComponent],
    entryComponents:[ComunicadoComponent,DetalleComponent,ModalPdfComponent]
})

export class OrdenPagoModule { }

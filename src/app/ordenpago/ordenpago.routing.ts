import { Routes } from '@angular/router';
import { OrdenPagoBuscarComponent } from './buscar/ordenpagobuscar.component';
export const OrdenPagoRoutes: Routes = [
  {

    path: '',
    children: [
     
     {
        path: 'buscar/:op',
        component: OrdenPagoBuscarComponent
      }  
    ]
  }
];

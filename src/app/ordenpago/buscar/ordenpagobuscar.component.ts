import { Router, ActivatedRoute, Params } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroupDirective, NgForm, FormGroup, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { FormBuilder } from '@angular/forms';
import { FormService } from 'src/app/services/form';
import { UserMS } from 'src/app/model/login';

import { AuthGuardService } from 'src/app/services/auth-guard.service';
import { OrdenPagoService } from 'src/app/services/ordenpago.service';
import { ToastrService } from 'ngx-toastr';
import { MatDialog } from '@angular/material/dialog';
import { ComunicadoComponent } from '../modal/comunicado.component';
import { DetalleComponent } from '../modal/detalle.component';
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

var oComponent: OrdenPagoBuscarComponent;
declare var $: any;
declare var moment: any;

@Component({
  moduleId: module.id,
  selector: 'ordenpagobuscar-cmp',
  templateUrl: 'ordenpagobuscar.component.html',
  providers: [OrdenPagoService]
})
export class OrdenPagoBuscarComponent implements OnInit {

  filtrosForm: FormGroup;
  matcher = new MyErrorStateMatcher();
  public IDPerfil;
  public RUC = null;
  public user: UserMS;
  public ListAux = [];
  public ListDetalle = [];
  public List = [];
  public Today = new Date();
  public FeIni = new Date();
  public ordenPago;
  public montoTotal;
  public fechaEmision;
  public moneda;
  public titulo;
  public disable = false;
  public op;
  public Empresas = [
    { nombre: "Naviera Transoceánica S.A", correo: "facturacionreceptor1@navitranso.com", ruc: "20522163890" },
    { nombre: "Petrolera Transoceánica S.A.", correo: "facturacionreceptor@petranso.com", ruc: "20100126606" },
    { nombre: "Naviera Petral S.A", correo: "facturacionreceptor@petral.com.pe", ruc: "20511922578" }
  ]
  public clase4;
  constructor(private router: Router, private activatedRoute: ActivatedRoute,
    public orden: OrdenPagoService,
    public toastr: ToastrService,
    public _modalDialog: MatDialog,
    public fb: FormBuilder, public FormService: FormService, public auth: AuthGuardService) {
    if (window.screen.width > 1000) {
      this.clase4 = "table table-no-bordered table-striped";

    }
    else {
      this.clase4 = "table table-no-bordered table-striped table-responsive-stack";

    }
    this.user = JSON.parse(window.localStorage.getItem("user")) as UserMS;
    this.IDPerfil = this.user.user.CodRol;
  }

  CrearFormulario() {
    this.filtrosForm = this.fb.group({
      RUC: [{ value: oComponent.RUC ? oComponent.RUC : '', disabled: false }, [Validators.required, Validators.pattern("^[0-9]{11}$")]],
      FechaInicio: [{ value: oComponent.FeIni ? moment(oComponent.FeIni).format('YYYY-MM-DD') : '', disabled: false }, []],
      FechaFin: [{ value: oComponent.Today ? moment(oComponent.Today).format('YYYY-MM-DD') : '', disabled: false }, []],
      Sociedad: [{ value: '', disabled: false }, []]

    });
  }
  public navigate(nav) {
    this.router.navigate(nav, { relativeTo: this.activatedRoute });
  }
  ngOnInit() {
    oComponent = this;
    this.activatedRoute.params.subscribe((params: Params) => {
      this.op = params['op'];
    });
    if (this.op == 0) {
      if (window.screen.width > 1200) {
        this.modalComunicado('46%');
      }
      else {
        this.modalComunicado('91%');
      }
    }
    this.FeIni.setDate(this.Today.getDate() - 30);
    if (this.IDPerfil != '0022') {
      this.titulo = "CONSULTE SU PAGO POR RANGO DE FECHAS"
      this.RUC = this.user.user_id;
      this.CrearFormulario();
      this.Buscar();
    }
    else {
      this.titulo = "CONSULTE PAGO DE PROVEEDORES"
      this.CrearFormulario();
    }
  }

  Buscar() {
    let diferencia;
    var fecha1 = moment(oComponent.filtrosForm.value["FechaInicio"]);
    var fecha2 = moment(oComponent.filtrosForm.value["FechaFin"]);
    diferencia = fecha2.diff(fecha1, 'days');
    this.List = [];
    let item = {
      Ruc: oComponent.filtrosForm.value["RUC"] ? oComponent.filtrosForm.value["RUC"] : this.RUC,
      FechaInicio: oComponent.filtrosForm.value["FechaInicio"] ? moment(oComponent.filtrosForm.value["FechaInicio"]).format('DD/MM/YYYY') : moment(oComponent.FeIni).format('DD/MM/YYYY'),
      FechaFin: oComponent.filtrosForm.value["FechaFin"] ? moment(oComponent.filtrosForm.value["FechaFin"]).format('DD/MM/YYYY') : moment(oComponent.Today).format('DD/MM/YYYY'),
      Sociedad: oComponent.filtrosForm.value["Sociedad"] ? oComponent.filtrosForm.value["Sociedad"] : '',
    }
    this.FormService.markFormGroupTouched(this.filtrosForm);
    if (this.filtrosForm.valid) {
      if (diferencia > 31) {
        this.toastr.info(("El rango de fechas no debe ser mayor a 31 días calendarios").toUpperCase());
        return;
      }
      else {
        this.disable = true;
        this.orden.GetOrdenPago(item).subscribe((res) => {
          if (window.screen.width < 1000) {
            this.inicio();
          } this.ListAux = res;
          if (this.ListAux.length == undefined) {
            this.toastr.info("NO EXISTEN REGISTROS QUE MOSTRAR")
          }
          else if (this.ListAux[0].stcd1 == "99999999991") {
            this.toastr.info("EL RUC NO EXISTE")
          }
          else if (this.ListAux[0].stcd1 == "99999999992") {
            this.toastr.info("NO EXISTEN REGISTROS QUE MOSTRAR")
          }
          else {
            if (item.Sociedad == '') {
              this.ListAux.forEach(element => {
                element["fechaEmision"] = moment(element.zaldt).format("DD/MM/YYYY");
                if (element.rzawe == "C") {
                  element["tipoPago"] = 'CHEQUE';
                  element["tipoPagoRe"] = 'SE LE PAGÓ CON CHEQUE DE GERENCIA, FAVOR ACERCARSE AL ' + element.banka + ' CON EL CARGO DE SU FACTURA.'
                }
                if (element.rzawe == "A") {
                  element["tipoPago"] = 'ABONO EN CUENTA';
                }
                if (element.rzawe == "E") {
                  element["tipoPago"] = 'EFECTIVO';
                }
                if (element.rzawe == "L") {
                  element["tipoPago"] = 'LETRA';
                }
                if (element.rzawe == "F") {
                  element["tipoPago"] = 'TRANSFERENCIA BANCARIA - FACT.';
                }
                if (element.rzawe == "G") {
                  element["tipoPago"] = 'TRANSFERENCIA BANCARIA - FID.';
                }
                if (element.rzawe == "P") {
                  element["tipoPago"] = 'TRANSFERENCIA JP MORGAN';
                }
                if (element.rzawe == "T") {
                  element["tipoPago"] = 'TRANSFERENCIA BANCARIA';
                }
                if (element.rzawe == "V") {
                  element["tipoPago"] = 'TRANSFERENCIA BANCARIA ENTRANTE';
                }
                this.List.push(element);
              });
            }
            else {
              this.List = [];
              this.ListAux.forEach(element => {
                if (element.ruc_adqui == item.Sociedad) {
                  element["fechaEmision"] = moment(element.zaldt).format("DD/MM/YYYY");
                  if (element.rzawe == "C") {
                    element["tipoPago"] = 'CHEQUE';
                    element["tipoPagoRe"] = 'SE LE PAGÓ CON CHEQUE DE GERENCIA, FAVOR ACERCARSE AL BANCO ' + element.banka + ' CON EL CARGO DE SU FACTURA'
                  }
                  if (element.rzawe == "E") {
                    element["tipoPago"] = 'EFECTIVO';
                  }
                  if (element.rzawe == "A") {
                    element["tipoPago"] = 'ABONO EN CUENTA';
                  }
                  if (element.rzawe == "L") {
                    element["tipoPago"] = 'LETRA';
                  }
                  if (element.rzawe == "F") {
                    element["tipoPago"] = 'TRANSFERENCIA BANCARIA - FACT.';
                  }
                  if (element.rzawe == "G") {
                    element["tipoPago"] = 'TRANSFERENCIA BANCARIA - FID.';
                  }
                  if (element.rzawe == "P") {
                    element["tipoPago"] = 'TRANSFERENCIA JP MORGAN';
                  }
                  if (element.rzawe == "T") {
                    element["tipoPago"] = 'TRANSFERENCIA BANCARIA';
                  }
                  if (element.rzawe == "V") {
                    element["tipoPago"] = 'TRANSFERENCIA BANCARIA ENTRANTE';
                  }
                  this.List.push(element);
                }
              });
              if (this.List.length == 0) {
                this.toastr.info("NO EXISTEN REGISTROS QUE MOSTRAR")
              }
            }
          }
          this.disable = false;
        });
      }
    }
  }
  public tipoPago;
  public banka;

  limpiar() {
    if (this.IDPerfil != '0022') {
      this.RUC = this.user.user_id;
      this.CrearFormulario();
    }
    else {
      this.filtrosForm = this.fb.group({
        RUC: [{ value: '', disabled: false }, [Validators.required, Validators.pattern("^[0-9]{11}$")]],
        FechaInicio: [{ value: oComponent.FeIni ? moment(oComponent.FeIni).format('YYYY-MM-DD') : '', disabled: false }, []],
        FechaFin: [{ value: oComponent.Today ? moment(oComponent.Today).format('YYYY-MM-DD') : '', disabled: false }, []],
        Sociedad: [{ value: '', disabled: false }, []]
      });
    }
  }
  imprimir(e) {
    this.ListDetalle = [];
    this.ordenPago = e.opago;
    this.fechaEmision = moment(e.zaldt).format("DD/MM/YYYY");
    this.tipoPago = e.tipoPago;
    this.banka = e.banka;
    var o = JSON.parse(e.detalle);
    this.ListDetalle = o;
    this.ListDetalle.forEach(element => {
      element.bldat = moment(element.bldat).format("DD/MM/YYYY");
    });
    this.montoTotal = e.rwbtr;
    this.moneda = e.waers;
    $('#print-section-material').printThis({
    });
  }
  public inicio() {
    $(document).ready(function () {
      $('.table-responsive-stack').find("th").each(function (i) {
        $('.table-responsive-stack td:nth-child(' + (i + 1) + ')').prepend('<span class="table-responsive-stack-thead">' + $(this).text() + ':</span> ');
        $('.table-responsive-stack-thead').hide();
      });
      $('.table-responsive-stack').each(function () {
        var thCount = $(this).find("th").length;
        var rowGrow = 100 / thCount + '%';
        $(this).find("th, td").css('flex-basis', rowGrow);
      });
      function flexTable() {
        if ($(window).width() < 768) {
          $(".table-responsive-stack").each(function (i) {
            $(this).find(".table-responsive-stack-thead").show();
            $(this).find('thead').hide();
          });
        }
        else {
          $(".table-responsive-stack").each(function (i) {
            $(this).find(".table-responsive-stack-thead").hide();
            $(this).find('thead').show();
          });
        }
        // flextable   
      }
      flexTable();
      window.onresize = function (event) {
        flexTable();
      };
      // document ready  
    });
  }
  public Detalle(e) {
    if (window.screen.width > 1000) {
      var o = JSON.parse(e.detalle);
      if (o.length < 3) {
        this.modalDetalle(e, '80%', '65%');
      }
      else {
        this.modalDetalle(e, '80%', '75%');
      }
    }
    else {
      this.modalDetalle(e, '90%', '85%');
    }
  }
  public modalComunicado(ancho) {
    var dialogRef = this._modalDialog.open(ComunicadoComponent, {
      maxWidth: ancho,
      height: '76%',
      autoFocus: false,
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(function (result) {
      if (result == 1) {
      }
    });
  }
  public modalDetalle(op, ancho, largo) {
    var aux = {
      RUC: oComponent.filtrosForm.value["RUC"],
      FechaInicio: moment(oComponent.filtrosForm.value["FechaInicio"]).format('DD-MM-YYYY'),
      FechaFin: moment(oComponent.filtrosForm.value["FechaFin"]).format('DD-MM-YYYY')
    };
    var dialogRef = this._modalDialog.open(DetalleComponent, {
      maxWidth: ancho,
      height: largo,
      autoFocus: false,
      disableClose: true
    });
    dialogRef.componentInstance.op = op;
    dialogRef.componentInstance.aux = aux;
    dialogRef.afterClosed().subscribe(function (result) {
      if (result == 1) {
      }
    });
  }
}


import { environment } from 'src/environments/environment';


export const BASE_URL_WS = environment.BASE_URL_WS + environment.NAME;
export const MENSAJE_ERROR_REGISTRO = '¡OCURRIÓ UN ERROR!. POR FAVOR VERIFIQUE Y COMPLETE CORRECTAMENTE LOS DATOS REQUERIDOS';


/******************LOGIN********************** */
// export const URL_AUTH_BASE = BASE_URL_WS + 'maestros/';
export const URL_AUTH_BASE = BASE_URL_WS + 'login/';
export const URL_LOGIN = URL_AUTH_BASE + 'authenticate';
export const URL_REGISTRO = URL_AUTH_BASE + 'registro';
export const URL_RECUPERAR = URL_AUTH_BASE + 'recuperar';
export const URL_CAMBIAR_PASS = URL_AUTH_BASE + 'cambiarpassword';

/******************LOGIN********************** */



/******************CREAR Y CAMBIAR CUENTA********************** */
export const URL_ORDEN_PAGO= BASE_URL_WS + 'ordenpago/new';
export const URL_ORDEN_PDF= BASE_URL_WS + 'ordenpago/pdf';
export const URL_ORDEN_PDF2= BASE_URL_WS + 'ordenpago/pdf/base64';
export const URL_RUC= BASE_URL_WS + 'ruc';
export const URL_RUC_BUSCAR= URL_RUC + '/datatable';
export const URL_RUC_PROVEEDOR= URL_RUC + '/proveedor';
export const URL_FACTURA= BASE_URL_WS + 'estadofactura';







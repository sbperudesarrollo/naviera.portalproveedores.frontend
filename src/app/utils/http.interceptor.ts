import { HttpInterceptor, HttpHandler, HttpRequest, HttpEvent, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from "@angular/core"
import { Observable, of } from "rxjs";
import { tap, catchError } from "rxjs/operators";
import { ToastrService } from 'ngx-toastr';
@Injectable()
export class AppHttpInterceptor implements HttpInterceptor {
    constructor(public toasterService: ToastrService) { }
    intercept(
        req: HttpRequest<any>,
        next: HttpHandler
    ): Observable<HttpEvent<any>> {

        return next.handle(req).pipe(
            tap(evt => {
                if (evt instanceof HttpResponse) {
                    if (evt.body && evt.body.success)
                        console.log("OK");
                }
            }),
            catchError((err: any) => {
                if (err instanceof HttpErrorResponse) {
                    try {
                        console.log("error1");
                        this.toasterService.info("EL ARCHIVO DE COMPROBANTE DE RETENCIÓN NO FUE ENCONTRADO");
                    } catch (e) {
                        console.log("error2");
                        this.toasterService.warning('Al parecer ocurrió un error. Contáctese con el Administrador del Sistema');
                    }
                    //log error 
                }
                return of(err);
            }));

    }

}
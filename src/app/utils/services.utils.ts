import { Injectable } from '@angular/core';
import { HttpErrorResponse,HttpHeaders } from '@angular/common/http';
import swal from 'sweetalert2';

import { throwError } from 'rxjs';

import { UserMS } from 'src/app/model/login';

declare var $: any;

@Injectable()
export class ServicesUtils {

    constructor() { }
    public getHeaderLogin() {
        let user = JSON.parse(window.localStorage.getItem("user")) as UserMS;
        let httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
               
               /* 'usuario_id': user._id,
                'organizacion_id': user.organizacion.id,
                'tipo_organizacion': user.organizacion.tipo,
                'token': user.token*/
            })


        };
        return httpOptions.headers;
    }
    public getHeaderAPI(){
        let httpOptions = {
            headers: new HttpHeaders({
                'Access-Control-Allow-Origin': '*',
                'Content-Type': 'application/json',
                'Access-Control-Allow-Methods': 'POST, GET, PUT, DELETE, OPTIONS',
                'Access-Control-Allow-Headers': 'X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept'
            })


        };
        return httpOptions.headers;

    }
    public getHeader() {
        let user = JSON.parse(window.localStorage.getItem("user")) as UserMS;
        let token =user.token;
        let httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
               
               /* 'usuario_id': user._id,
                'organizacion_id': user.organizacion.id,
                'tipo_organizacion': user.organizacion.tipo,
                'token': user.token*/
            }).set( 'Authorization',  `Bearer ${token}`)


        };
        return httpOptions.headers;
    }

    public getHeaderFile() {
        let user = JSON.parse(window.localStorage.getItem("user")) as UserMS;
        let token =user.token;
        let httpOptions = {       
            headers: new HttpHeaders({             
            }).set( 'Authorization',  `Bearer ${token}`)


        };
        return httpOptions.headers;
    }
    public getHeaderWithOutToken() {
       
        let httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
               
               /* 'usuario_id': user._id,
                'organizacion_id': user.organizacion.id,
                'tipo_organizacion': user.organizacion.tipo,
                'token': user.token*/
            })


        };
        return httpOptions.headers;
    }
    public getHeader2(token) {
        let user = JSON.parse(window.localStorage.getItem("user")) as UserMS;
        const headers = new HttpHeaders({
            
           
          }).set( 'Authorization',  `Bearer ${token}`);
        return headers;
    }

    public extractData(res: Response) {
        // debugger;
        let body = res;
        // console.log("ExtractData: ", res)
        return body || {};
    }


}

import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
//import {UtilsModule} from '../utils/utils.module';
import { MaterialModule } from '../app.module';



import { FormService } from '../services/form';

import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatInputModule } from '@angular/material/input';
import { InfoUsuarioRoutes } from './infousuario.routing';
import { InfoUsuarioComponent } from './formulario/infousuario.component';

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(InfoUsuarioRoutes),
        FormsModule,
        ReactiveFormsModule,
        MaterialModule,
        MatCardModule,
        MatInputModule,
        MatButtonModule
    ],
    providers: [FormService],
    declarations: [
        InfoUsuarioComponent],
})

export class InfoUsuarioModule { }

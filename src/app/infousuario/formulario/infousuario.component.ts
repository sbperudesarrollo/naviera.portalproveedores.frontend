import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { UserMS } from 'src/app/model/login';
import { AuthGuardService } from 'src/app/services/auth-guard.service';


var oComponent: InfoUsuarioComponent;

@Component({
  moduleId: module.id,
  selector: 'infousuario-cmp',
  templateUrl: 'infousuario.component.html',
})
export class InfoUsuarioComponent implements OnInit {

  public user: UserMS;

  constructor(private router: Router, private activatedRoute: ActivatedRoute,
    public auth: AuthGuardService) {
    this.user = JSON.parse(window.localStorage.getItem("user")) as UserMS;
  }

  public navigate(nav) {
    this.router.navigate(nav, { relativeTo: this.activatedRoute });
  }
  ngOnInit() {
    oComponent = this;
  }


}


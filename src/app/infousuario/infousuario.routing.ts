import { Routes } from '@angular/router';
import { InfoUsuarioComponent } from './formulario/infousuario.component';
export const InfoUsuarioRoutes: Routes = [
  {

    path: '',
    children: [
     
     {
        path: 'formulario',
        component: InfoUsuarioComponent
      }  
    ]
  }
];

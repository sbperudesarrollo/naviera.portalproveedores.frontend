import { Routes } from '@angular/router';

import { AdminLayoutComponent } from './layouts/admin/admin-layout.component';
import { AuthLayoutComponent } from './layouts/auth/auth-layout.component';
import { AuthGuardService } from './services/auth-guard.service';

export const AppRoutes: Routes = [
    {
        path: '',
        redirectTo: 'pages/login',
        pathMatch: 'full',
    }, {
        path: '',
        component: AdminLayoutComponent,
        canActivateChild: [AuthGuardService],
        children: [
            {
                path: 'ordenpago',
                loadChildren: './ordenpago/ordenpago.module#OrdenPagoModule'
            },
            {
                path: 'estadofactura',
                loadChildren: './estadofactura/estadofactura.module#EstadoFacturaModule'
            }
            ,
            {
                path: 'infousuario',
                loadChildren: './infousuario/infousuario.module#InfoUsuarioModule'
            },
            {
                path: 'ayuda',
                loadChildren: './ayuda/ayuda.module#AyudaModule'
            },
            {
                path: 'proveedores',
                loadChildren: './proveedores/proveedores.module#ProveedoresModule'
            },
            {
                path: 'menu',
                loadChildren: './menu/menu.module#MenuModule'
            }
        ]
    }, {
        path: '',
        component: AuthLayoutComponent,
        children: [{
            path: 'pages',
            loadChildren: './pages/pages.module#PagesModule'
        }]
    }
];

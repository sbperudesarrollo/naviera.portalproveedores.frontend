import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../app.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// import { FlexLayoutModule } from '@angular/flex-layout';

import { PagesRoutes } from './pages.routing';

import { LoginComponent } from './login/login.component';
import { RegistroComponent } from './registro/registro.component';
import { MatInputModule } from '@angular/material/input';
import { ContrasenaComponent } from './contrasena/contrasena.component';
import { FormService } from '../services/form';
import { ContactoComponent } from './contacto/contacto.component';
import { RecuperarContrasenaComponent } from './recuperarcontrasena/recuperarcontrasena.component';
import { BlockUIModule } from 'ng-block-ui';
import { BlockUIHttpModule } from 'ng-block-ui/http';
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(PagesRoutes),
    FormsModule,
    MaterialModule,
    ReactiveFormsModule,
    BlockUIModule.forRoot({
      message: "Cargando..."
    }),
    BlockUIHttpModule.forRoot(),
  ],
  declarations: [
    LoginComponent,
    RegistroComponent,
    ContrasenaComponent,
    ContactoComponent,
    RecuperarContrasenaComponent
  ],
  providers: [FormService]
})

export class PagesModule {}

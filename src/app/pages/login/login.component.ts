import { Component, OnInit, ElementRef, OnDestroy } from '@angular/core';
import { LoginService } from 'src/app/services/login.service';
import { LoginMS, UserMS } from 'src/app/model/login';
import { Router, ActivatedRoute, Params } from '@angular/router';
import swal from 'sweetalert2';
import { NgBlockUI, BlockUI } from 'ng-block-ui';
declare var $: any;
var oComponent: LoginComponent;

@Component({
    selector: 'app-login-cmp',
    templateUrl: './login.component.html',
    providers: [LoginService]
})

export class LoginComponent implements OnInit, OnDestroy {
    private toggleButton: any;
    private sidebarVisible: boolean;
    public item: LoginMS;
    test: Date = new Date();
    @BlockUI() blockUI: NgBlockUI;
    constructor(private router: Router, private activatedRoute: ActivatedRoute, private loginService: LoginService, private element: ElementRef) {
        this.sidebarVisible = false;
        oComponent = this;
        this.item = new LoginMS();

    }
    public navigate(nav) {
        this.router.navigate(nav, { relativeTo: this.activatedRoute });
    }
    ngOnInit() {
        var navbar: HTMLElement = this.element.nativeElement;
        this.toggleButton = navbar.getElementsByClassName('navbar-toggle')[0];
        const body = document.getElementsByTagName('body')[0];
        body.classList.add('login-page');
        body.classList.add('off-canvas-sidebar');
        const card = document.getElementsByClassName('card')[0];
        setTimeout(function () {
            // after 1000 ms we add the class animated to the login/register card
            card.classList.remove('card-hidden');
        }, 700);
    }
    sidebarToggle() {
        var toggleButton = this.toggleButton;
        var body = document.getElementsByTagName('body')[0];
        var sidebar = document.getElementsByClassName('navbar-collapse')[0];
        if (this.sidebarVisible == false) {
            setTimeout(function () {
                toggleButton.classList.add('toggled');
            }, 500);
            body.classList.add('nav-open');
            this.sidebarVisible = true;
        } else {
            this.toggleButton.classList.remove('toggled');
            this.sidebarVisible = false;
            body.classList.remove('nav-open');
        }
    }
    ngOnDestroy() {
        const body = document.getElementsByTagName('body')[0];
        body.classList.remove('login-page');
        body.classList.remove('off-canvas-sidebar');
    }
    LogIn(event) {

        if (!this.item.username || this.item.username.trim() == '') {

            swal({
                text: 'El usuario es un campo requerido.',//res.errorMessage,
                type: 'warning',
                buttonsStyling: false,
                confirmButtonClass: "btn btn-warning"
            });
            return;
        }
        if (!this.item.password || this.item.password.trim() == '') {

            swal({
                text: 'La contraseña es un campo requerido.',//res.errorMessage,
                type: 'warning',
                buttonsStyling: false,
                confirmButtonClass: "btn btn-warning"
            });
            return;
        }
        this.blockUI.start('Cargando...'); // Start blocking
        this.loginService.LogIn(this.item).subscribe((res) => {
            if (res.message_error && res.message_error != '') {
                this.blockUI.stop();
                swal({
                    text: 'El usuario o contraseña ingresados no son correctos.',
                    type: 'warning',
                    buttonsStyling: false,
                    confirmButtonClass: "btn btn-warning"
                });
                return;
            }
            else {
                this.blockUI.stop(); 
                let user = res as UserMS;
                user.user_id=user.user.CodUsuario;
                user.token = user.token;
                localStorage.setItem('user', JSON.stringify(user));
                let nav = ["/ordenpago/buscar", 0]
                oComponent.navigate(nav);
            }
        });
        event.preventDefault();

    }
}

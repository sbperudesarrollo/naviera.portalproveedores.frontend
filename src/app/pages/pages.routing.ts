import { Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegistroComponent } from './registro/registro.component';
import { ContrasenaComponent } from './contrasena/contrasena.component';
import { ContactoComponent } from './contacto/contacto.component';
import { RecuperarContrasenaComponent } from './recuperarcontrasena/recuperarcontrasena.component';

export const PagesRoutes: Routes = [

    {
        path: '',
        children: [ {
            path: 'login',
            component: LoginComponent
        }, {
            path: 'registro',
            component: RegistroComponent
        },
        {
            path: 'contacto',
            component: ContactoComponent
        },
        {
            path: 'formulario/:id',
            component: ContrasenaComponent
          },
          {
            path: 'recuperarcontrasena',
            component: RecuperarContrasenaComponent
          },
          
    ]
    }
];

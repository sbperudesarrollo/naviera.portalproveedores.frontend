import { Component, OnInit, ElementRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { LoginMS } from 'src/app/model/login';
import swal from 'sweetalert2';
import { FormGroup, FormControl, FormGroupDirective, NgForm, FormBuilder, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { FormService } from 'src/app/services/form';
import { LoginService } from 'src/app/services/login.service';
import { ToastrService } from 'ngx-toastr';

export class MyErrorStateMatcher implements ErrorStateMatcher {
    isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
        const isSubmitted = form && form.submitted;
        return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
    }
}

declare var $: any;
var oComponent: ContrasenaComponent;
declare var require: any
const FileSaver = require('file-saver');
@Component({
    selector: 'app-contrasena-cmp',
    templateUrl: './contrasena.component.html',
    providers: [LoginService]
})

export class ContrasenaComponent implements OnInit {
    usuarioForm: FormGroup;
    private toggleButton: any;
    private sidebarVisible: boolean;
    public item: LoginMS;
    test: Date = new Date();
    public id;
    public params = {
        CodUsuario: null,
        TipoPersona: 'J',
        Contraseña: null,
        RepetirContraseña: null,
        Correo: null,
        TipoUsuario: 'E',
        UsuarioCreacion: 'CCP',
        Origen_Registro: null
    }
    matcher = new MyErrorStateMatcher();
    constructor(private router: Router, private activatedRoute: ActivatedRoute, public fb: FormBuilder, public toastr: ToastrService,
        private element: ElementRef, private loginService: LoginService, public FormService: FormService) {
        this.sidebarVisible = false;
        oComponent = this;
        this.item = new LoginMS();
    }
    public navigate(nav) {
        this.router.navigate(nav, { relativeTo: this.activatedRoute });
    }
    CrearFormulario() {

        this.usuarioForm = this.fb.group({
            Contrasena: ['', [Validators.required, Validators.pattern("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[A-Za-z\\d]{6,15}$")]],
            ConfirmarContrasena: ['', [Validators.required, Validators.pattern("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[A-Za-z\\d]{6,15}$")]]
        });
    }
    ngOnInit() {
        oComponent = this;
        this.activatedRoute.params.subscribe((params: Params) => {
            this.id = params['id'];
        });
        this.item.username = this.id;
        this.CrearFormulario();
    }
    registrarse(id) {
        let params = {
            IDProveedor: id
        }
        this.loginService.Update(params).subscribe((data) => {
            if (data.status == 1) {
                console.log("OK");
            }
            else {
                console.log(data);
            }
        });
    }
    public IDProveedor;
    getUsuario() {
        let params = {
            RUC: this.item.username
        }
        this.loginService.GetProveedor(params).subscribe((data) => {
            if (data.user == null) {
                this.toastr.info("EL USUARIO / RUC NO EXISTE");
            }
            else {
                this.IDProveedor = data.user.IDProveedor;
                this.params.CodUsuario = this.id;
                this.params.Correo = data.user.Correo1;
                this.params.Contraseña = this.usuarioForm.value["Contrasena"];
                this.params.RepetirContraseña = this.usuarioForm.value["ConfirmarContrasena"];
                if (data.user.Estado == "SOLICITADO") {
                    this.registrarProveedor();
                }
                else {
                    this.toastr.info("USUARIO YA REGISTRADO");
                }
            }
        });
    }
    registrarProveedor() {
        this.FormService.markFormGroupTouched(this.usuarioForm);
        if (this.usuarioForm.valid) {
            if (this.usuarioForm.value["Contrasena"] != this.usuarioForm.value["ConfirmarContrasena"]) {
                this.toastr.info("CONFIRMAR CORRECTAMENTE LA CONTRASEÑA");
            }
            else {
                this.loginService.RegistroProveedor(this.params).subscribe((data) => {
                    if (data.status == 0) {
                        this.registrarse(this.IDProveedor);
                        swal({
                            text: "EL REGISTRO FUE INGRESADO CON ÉXITO",
                            type: 'success',
                            buttonsStyling: false,
                            confirmButtonClass: "btn btn-success"
                        }).then(
                            (result) => {
                                let nav = ["/pages/login/"];
                                oComponent.navigate(nav);
                            }, (dismiss) => {
                                let nav = ["/pages/login/"];
                                oComponent.navigate(nav);
                            });
                    }
                    else {
                        this.toastr.info((data.message_error).toUpperCase());
                    }
                });
            }

        }
        else {
            return false;
        }
    }
}

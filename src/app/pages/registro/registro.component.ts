import { Component, OnInit, ElementRef, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import swal from 'sweetalert2';
import { LoginService } from 'src/app/services/login.service';
import { ToastrService } from 'ngx-toastr';
var oComponent: RegistroComponent;

@Component({
    selector: 'app-registrousuario-cmp',
    templateUrl: './registro.component.html',
    providers: [LoginService]
})

export class RegistroComponent implements OnInit, OnDestroy {
    private toggleButton: any;
    private sidebarVisible: boolean;
    public usuario = "";
    public razon;
    public list = [];
    public show = 0;
    public check;
    public correoConf;
    test: Date = new Date();
    public showBoton = 0;
    constructor(private router: Router, private activatedRoute: ActivatedRoute, public toastr: ToastrService, private loginService: LoginService, private element: ElementRef) {
        this.sidebarVisible = false;
        oComponent = this;

    }
    public navigate(nav) {
        this.router.navigate(nav, { relativeTo: this.activatedRoute });
    }
    ngOnInit() {
        var navbar: HTMLElement = this.element.nativeElement;
        this.toggleButton = navbar.getElementsByClassName('navbar-toggle')[0];
        const body = document.getElementsByTagName('body')[0];
        body.classList.add('login-page');
        body.classList.add('off-canvas-sidebar');
        const card = document.getElementsByClassName('card')[0];
        setTimeout(function () {
            card.classList.remove('card-hidden');
        }, 700);
    }
    sidebarToggle() {
        var toggleButton = this.toggleButton;
        var body = document.getElementsByTagName('body')[0];
        var sidebar = document.getElementsByClassName('navbar-collapse')[0];
        if (this.sidebarVisible == false) {
            setTimeout(function () {
                toggleButton.classList.add('toggled');
            }, 500);
            body.classList.add('nav-open');
            this.sidebarVisible = true;
        } else {
            this.toggleButton.classList.remove('toggled');
            this.sidebarVisible = false;
            body.classList.remove('nav-open');
        }
    }
    ngOnDestroy() {
        const body = document.getElementsByTagName('body')[0];
        body.classList.remove('login-page');
        body.classList.remove('off-canvas-sidebar');
    }
    validar() {
        let params = "/" + this.usuario
        this.loginService.RUC(params).subscribe((data) => {
            if (data[0].stcd1 != "99999999999") {
                this.list = data;
                this.razon = data[0].name1;
                this.reducirCorreo(data[0].correo);
                this.show = 1;
            }
            else {
                this.show = 0;
                this.toastr.info("EL USUARIO / RUC NO EXISTE");
            }
        });

    }
    reducirCorreo(correo) {
        let aux = ' ';
        let aux2 = '';
        for (let index = 0; index < 3; index++) {
            const element = correo.charAt(index);
            aux += element;
        }
        for (let i = (correo.length - 10); i < correo.length; i++) {
            const e = correo.charAt(i);
            aux2 += e;
        }
        this.correoConf = aux + '*****' + aux2
    }
    obtenerClave(id) {
        if (this.list[0].correo == '') {
            this.toastr.info("SU CORREO NO ESTÁ REGISTRADO, POR FAVOR COMUNÍQUESE CON EL ÁREA DE TESORERÍA O VEA LA GUÍA DE ACCESO.");
            let nav = ["/pages/contacto/"];
            oComponent.navigate(nav);
        }
        else {
            let params = {
                IDProveedor: id,
                RUC: this.usuario,
                Correo: this.list[0].correo,
                RazonSocial: this.razon,
                Correo1: this.list[0].correo,
                Correo2: '',
                Apellido: this.list[0].name2
            }
            this.loginService.SolicitarClave(params).subscribe((data) => {
                if (data.status == 1) {
                    swal({
                        text: 'SE HA ENVIADO UN CORREO DE CONFIRMACIÓN DE REGISTRO ONLINE A SU CORREO EMPRESARIAL',
                        type: 'success',
                        buttonsStyling: false,
                        confirmButtonClass: "btn btn-success"
                    }).then(
                        (result) => {
                            let nav = ["/pages/login/"];
                            oComponent.navigate(nav);
                        }, (dismiss) => {
                            let nav = ["/pages/login/"];
                            oComponent.navigate(nav);
                        });
                }
                else {
                    this.toastr.info("OCURRIÓ UN ERROR, VOLVER A INTENTAR");
                    this.showBoton = 0;
                }
            });
        }

    }
    public mensaje;
    getUsuario() {
        this.mensaje = 0;
        if (this.check == false || this.check == undefined) {
            this.mensaje = 1;
            return;
        }
        else {
            let id;
            this.showBoton = 1;
            let params = {
                RUC: this.usuario
            }
            this.loginService.GetProveedor(params).subscribe((data) => {
                if (data.user == null) {
                    id = 0;
                }
                else {
                    id = data.user.IDProveedor;
                }
                this.obtenerClave(id);
            });
        }
    }
}

import { Component, OnInit, ElementRef } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { LoginMS } from 'src/app/model/login';
import swal from 'sweetalert2';
import { FormGroup, FormControl, FormGroupDirective, NgForm, FormBuilder, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { FormService } from 'src/app/services/form';
import { LoginService } from 'src/app/services/login.service';
import { ToastrService } from 'ngx-toastr';

export class MyErrorStateMatcher implements ErrorStateMatcher {
    isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
        const isSubmitted = form && form.submitted;
        return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
    }
}

declare var $: any;
var oComponent: RecuperarContrasenaComponent;
declare var require: any
const FileSaver = require('file-saver');
@Component({
    selector: 'app-recuperarcontrasena-cmp',
    templateUrl: './recuperarcontrasena.component.html',
    providers: [LoginService]
})

export class RecuperarContrasenaComponent implements OnInit {
    private toggleButton: any;
    private sidebarVisible: boolean;
    public usuario = "";
    public showBoton=0;
    test: Date = new Date();
    public params = {
        P_parametro: null
    }
    matcher = new MyErrorStateMatcher();
    constructor(private router: Router, private activatedRoute: ActivatedRoute,
        public toastr: ToastrService, private loginService: LoginService, public fb: FormBuilder, private element: ElementRef, public FormService: FormService) {
        this.sidebarVisible = false;
        oComponent = this;
    }
    public navigate(nav) {
        this.router.navigate(nav, { relativeTo: this.activatedRoute });
    }
    ngOnInit() {
    }
    registrarse() {
        this.showBoton=1;
        this.params.P_parametro = this.usuario;
        this.loginService.RecuperarContraseña(this.params).subscribe((data) => {
            if (data.status == 0) {
                swal({
                    text: (data.message_error).toUpperCase(),
                    type: 'success',
                    buttonsStyling: false,
                    confirmButtonClass: "btn btn-primary"
                }).then(
                    (result) => {
                        let nav = ["/pages/login/"];
                        oComponent.navigate(nav);
                    }, (dismiss) => {
                        let nav = ["/pages/login/"];
                        oComponent.navigate(nav);
                    });
            }
            else {
                this.showBoton=0;
                if (data.message_error==='El proceso no se pudo completar. El campo P_parametro debe ser un RUC,DNI ó correo.') {
                    this.toastr.info(("Debe ingresar RUC,DNI ó correo").toUpperCase());
                }
                else {
                    this.toastr.info((data.message_error).toUpperCase());
                }
            }
        });
    }
}
